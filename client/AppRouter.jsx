import React,  {Component} from 'react';
import { HashRouter, Route, Switch} from 'react-router-dom';
import Signin from './components/Signin.jsx';
import Signup from './components/Signup.jsx';
import GoogleSearch from './components/GoogleSearch.jsx';
import WomenCategoryWebView from './components/WomenCategoryWebView.jsx';
import mHome from './components/MobileAppHome.jsx';
import WomenCategory from './components/WomenCategory.jsx';
import PageNotFound from './components/PageNotFound.jsx';
import ProductDetails from './components/ProductDetails.jsx';
import InfoPage from './components/InfoPage.jsx';
import fb from './components/fbAuth.jsx';
import ArView from './components/ArView.jsx';
import RecommendedAndCelebrities from './components/RecommendedAndCelebrities.jsx';

export default class AppRouter extends Component{
  constructor(props){
    super(props);
  }

  render(){
    return(
      <HashRouter>
      <Switch>
        <Route path='/' component={GoogleSearch} exact/>
        <Route path='/signin' component={Signin} />
        <Route path='/signup' component={Signup} />
        <Route path='/mHome' component={mHome}/>
        <Route path='/womenCategory' component={WomenCategoryWebView}/>
        <Route path='/mWomenCategory/:category' component={WomenCategory}/>
        <Route path='/productDetails/:topName' component={ProductDetails}/>
        <Route path='/infoPage' component={InfoPage}/>
        <Route path='/RecommendedAndCelebrities' component={RecommendedAndCelebrities}/>
        <Route path='/ArView' component={ArView}/>
        <Route path='/fb' component={fb}/>

        <Route component={PageNotFound}/>
      </Switch>
      </HashRouter>
    );
  }
};
