import React, {Component} from 'react';
import {  Grid, Accordion, Icon ,List, Image} from "semantic-ui-react";
import { Link } from 'react-router-dom';
import Appbar from './AppBar.jsx';
import "../styles/style.css";

export default class InfoPage extends Component {
  constructor() {
    super();
    this.state={
       activeIndex: 0
      }
  }

  handleClick(e, titleProps) {
    console.log('--->',titleProps);
    var index  = titleProps;
    var activeIndex  = this.state;
   var newIndex = activeIndex === index ? -1 : index
   console.log(newIndex);
   this.setState({ activeIndex: newIndex },function(){
     console.log('state',this.state.activeIndex);
   })

  //    this.setState({activeIndex:titleProps});
    }

  render() {
    const { activeIndex } = this.state;
    return (
      <div>
        <Grid>
          <Grid.Row/>
          <Grid.Row>
            <Grid.Column width={1}/>
            <Grid.Column width={2}>
              <Link to='/productDetails/top5'>
              <Icon name='arrow left' size='large' color='black'/>
              </Link>
            </Grid.Column>
            <Grid.Column width={11}>
              <h2><strong>Black Ombre Layered Top</strong></h2>
            </Grid.Column>
          </Grid.Row>
          <Grid.Row>
            <Grid.Column width={1}/>
            <Grid.Column width={14}>
            <Accordion fluid styled>
                  <Accordion.Title active={activeIndex === 0} index={0} onClick={this.handleClick.bind(this)}>
                    <Icon name='dropdown' />
                    INFO
                  </Accordion.Title>
                  <Accordion.Content active={activeIndex === 0}>
                    <p>Black Ombre Layered Top
                    <br/>
                    <strong>Colour:</strong> Black
                    <br/>
                    <strong>Item Code:</strong>1945688</p>
                    <p>Model wears size S</p>
                    <p>Add a touch of glamour to your look with this gorgeous black sleeve <strong>jumper</strong>. Wear with jeans and heels.</p>
                    <List bulleted>
                      <List.Item>Jumper</List.Item>
                      <List.Item>Fitted</List.Item>
                      <List.Item>Round Neck</List.Item>
                      <List.Item>Machine Washing</List.Item>
                      <List.Item>61% Cotton, 39% Viscose</List.Item>
                    </List>
                  </Accordion.Content>

                  <Accordion.Title active={activeIndex === 1} index={1} onClick={this.handleClick.bind(this)}>
                    <Icon name='dropdown' />
                    DELIVERY
                  </Accordion.Title>
                  <Accordion.Content active={activeIndex === 1}>
                    On time safe delivery.
                  </Accordion.Content>

                  <Accordion.Title active={activeIndex === 2} index={2} onClick={this.handleClick.bind(this)}>
                    <Icon name='dropdown' />
                    RETURN
                  </Accordion.Title>
                  <Accordion.Content active={activeIndex === 2}>
                    <p><strong>Returns are easy</strong></p>
                    <Image src='https://images-eu.ssl-images-amazon.com/images/G/31/returns/return-instructions1._CB352516716_.png' alt="" />
                    <Image src='https://images-eu.ssl-images-amazon.com/images/G/31/returns/return-instructions2._CB352516653_.png' alt="" />
                    <Image src='https://images-eu.ssl-images-amazon.com/images/G/31/returns/return-instructions3._CB352516616_.png' alt="" />
                    <Image src='https://images-eu.ssl-images-amazon.com/images/G/31/returns/return-instructions4._CB352516834_.png' alt="" />
                  </Accordion.Content>

                  <Accordion.Title active={activeIndex === 3} index={3} onClick={this.handleClick.bind(this)}>
                    <Icon name='dropdown' />
                    REVIEWS
                  </Accordion.Title>
                  <Accordion.Content active={activeIndex === 3}>
                  <List>
                    <List.Item>
                      <Icon name='user outline' />
                      <List.Content>
                        <List.Header as='a'>Rachel</List.Header>
                        <List.Description>Good material and came as expected.</List.Description>
                      </List.Content>
                    </List.Item>
                    <List.Item>
                      <Icon name='user outline' />
                      <List.Content>
                        <List.Header as='a'>Lindsay</List.Header>
                        <List.Description>Very beautiful and very soft material. I liked it much. Better than the picture and good quality..</List.Description>
                      </List.Content>
                      </List.Item>
                    </List>
                  </Accordion.Content>
                  <Accordion.Title active={activeIndex === 4} index={4} onClick={this.handleClick.bind(this)}>
                    <Icon name='dropdown' />
                    SUPPLY CHAIN DETAILS
                  </Accordion.Title>
                  <Accordion.Content active={activeIndex === 4}>
                    <p><strong>Returns are easy</strong></p>

                  </Accordion.Content>
                </Accordion>
            </Grid.Column>
            <Grid.Column width={1} />
          </Grid.Row>
        </Grid>
      </div>

    );
  }
}
