import React, {Component} from 'react';
import { Grid, Segment, Image, Modal, Header, Icon, Button, Container, List, Loader, Dimmer, Card } from 'semantic-ui-react';
import Slider from 'react-slick';
import AppBar from './AppBar.jsx';

export default class RecommendedAndCelebrities extends Component{
  constructor(props){
    super(props);
  }

render(){
  const settings = {
   // dots: true,
   // fade: true,
   // infinite: true,
   // speed: 500,
   // slidesToShow: 1,
   // slidesToScroll: 1
   speed: 500,
   slidesToShow: 1,
   slidesToScroll: 1,
   centerMode: true
 };
    return(
      <div style={{overflow:"hidden"}}>


        <AppBar/>

              <Grid>
                <Grid.Row style={{marginTop:'15%'}}>
                  <Grid.Column width={16}>
                    <center style={{letterSpacing:"1px", textTransform:"uppercase"}}>Recommended items..</center>
                  </Grid.Column>
                </Grid.Row>

                <Grid.Row  only="mobile">
                  <Grid.Column width={1}/>
                  <Grid.Column width={7}>
                    <Image src='./client/assets/Images/BlackBlouse.jpg' alt="" />
                    <div>
                      <center>
                        <p className="WomenCategoryWebViewTitle">Navy striped Blouse</p>
                      </center>
                      <center>
                        <p className="WomenCategoryWebViewPrice">$40.00</p>
                      </center>
                    </div>
                  </Grid.Column>
                  <Grid.Column width={7}>
                    <Image src='./client/assets/Images/BlackNeckTop.jpg' alt="" />
                    <div>
                      <center>
                        <p className="WomenCategoryWebViewTitle">Black Neck Top</p>
                      </center>
                      <center>
                        <p className="WomenCategoryWebViewPrice">$20.00</p>
                      </center>
                    </div>
                  </Grid.Column>
                  <Grid.Column width={1}/>
                </Grid.Row>

                <Grid.Row  only="mobile">
                  <Grid.Column width={1}/>
                  <Grid.Column width={7}>
                    <Image src='./client/assets/Images/GreyCheckedTop.jpg' alt="" />
                    <div>
                      <center>
                        <p className="WomenCategoryWebViewTitle">Grey Checked Top</p>
                      </center>
                      <center>
                        <p className="WomenCategoryWebViewPrice">$30.00</p>
                      </center>
                    </div>
                  </Grid.Column>
                  <Grid.Column width={7}>
                    <Image src='./client/assets/Images/RustPrintShirt.jpg' alt="" />
                    <div>
                      <center>
                        <p className="WomenCategoryWebViewTitle">Rust Print Shirt</p>
                      </center>
                      <center>
                        <p className="WomenCategoryWebViewPrice">$30.00</p>
                      </center>
                    </div>
                  </Grid.Column>
                  <Grid.Column width={1}/>
                </Grid.Row>
              </Grid>

                <Grid>
                <Grid.Row style={{marginBottom:"5%"}}>
                  <Grid.Column width={16}>
                      <center style={{letterSpacing:'1px', textTransform:"uppercase", fontWeight:"bold"}}>Celebrities Fashion</center>
                  </Grid.Column>
                </Grid.Row>
              </Grid>
                <Slider {...settings} style={{marginBottom:"5%"}}>
                <div>
                  <Card style={{width:"75%"}}>
                    <center><Image size="small"  src='/client/assets/Images/NavyPrintedShirt.jpg' /></center>
                    <Card.Content>
                      <center>Yellow Plain Top</center>
                      {/* <Card.Meta><span className='date'>Joined in 2015</span></Card.Meta> */}
                      {/* <Card.Description>Rachel Weisz is a British actress living in London.</Card.Description> */}
                    </Card.Content>
                    <Card.Meta><center>$35.00</center></Card.Meta>
                    {/* <Card.Content extra>
                      <a><Icon name='user' /></a>
                    </Card.Content> */}
                  </Card>
              </div>
              <div>
                  <Card style={{width:"75%"}}>
                    <center><Image size="small" src='/client/assets/Images/GreenSheeredTop.jpg' /></center>
                    <Card.Content>
                      <center>Yellow Plain Top</center>
                      {/* <Card.Meta><span className='date'>Joined in 2015</span></Card.Meta> */}
                      {/* <Card.Description>Rachel Weisz is a British actress living in London.</Card.Description> */}
                    </Card.Content>
                    <Card.Meta><center>$35.00</center></Card.Meta>
                    {/* <Card.Content extra>
                      <a><Icon name='user' /></a>
                    </Card.Content> */}
                  </Card>
                </div>
                <div>
                    <Card style={{width:"75%"}}>
                      <center><Image size="small" src='/client/assets/Images/PetiteRuffleTop.jpg' /></center>
                      <Card.Content>
                        <center>Yellow Plain Top</center>
                        {/* <Card.Meta><span className='date'>Joined in 2015</span></Card.Meta> */}
                        {/* <Card.Description>Rachel Weisz is a British actress living in London.</Card.Description> */}
                      </Card.Content>
                      <Card.Meta><center>$35.00</center></Card.Meta>
                      {/* <Card.Content extra>
                        <a><Icon name='user' /></a>
                      </Card.Content> */}
                    </Card>
                  </div>

            </Slider>
      </div>
    );
  }
}
