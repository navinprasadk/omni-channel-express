import React, {Component} from 'react';
import { Grid, Image,Button , Icon, Dropdown} from 'semantic-ui-react';
import {Link} from 'react-router-dom';
import Slider from 'react-slick';
import Appbar  from './AppBar.jsx';
import '../styles/style.css';
import ProductRecommended from './ProductRecommended.jsx';
import Cookies from 'universal-cookie';
const cookies = new Cookies();
export default class ProductDetails extends Component{
  constructor(props){
    super(props);
this.state={
  modalOpen:close,
  cart: 0,
  wishList: 0
}
  }
  componentDidMount()
  {
    console.log(this.props.match.params.topName);
  }
  productInfo(){
    this.setState({modalOpen:true})
  }
  updateButton(){
      cookies.set('flag', 1);
      var Label = this.state.cart + 1;
      this.setState({cart : Label})
  }
  updateIcon(){
      this.wishlist();
    cookies.set('flag1', 1);
  }
  wishlist(){
    console.log('inside new function');
    var wish = 0
    var wish1 = wish + 1;
      this.setState({wishList: wish1});
  }
render(){
  var options = [{
        key: 'SIZE GUIDE',
        text: 'SIZE GUIDE',
        value: 'SIZE GUIDE'
      }, {
        key: 'US 6',
        text: 'US 6',
        value: 'US 6'
      }, {
        key: 'US 7',
        text: 'US 7',
        value: 'US 7'
      }, {
        key: 'US 8',
        text: 'US 8',
        value: 'US 8'
      },{
        key: 'US 9',
        text: 'US 9',
        value: 'US 9'
      }
    ]
  var settings = {
      dots: true,
      marginBottom:'19%'
    }
    console.log('wish',this.state.wishList);
    var button_component=(<Button fluid color='orange' size='big' onClick={this.updateButton.bind(this)}>Add to bag</Button>);
    if(cookies.get('flag')==1){
      button_component = (<Button fluid color='green' size='big' onClick={this.updateButton.bind(this)}>Added</Button>)
    }
    var icon_component =(<Icon circular name='empty heart' size='large' style={{position:'absolute',zIndex:'100',marginTop:'23%',left:'82%',color:'black'}} onClick={this.updateIcon.bind(this)}/>);
    if(cookies.get('flag1') == 1){
      icon_component =(<Icon circular name='heart' size='large' style={{position:'absolute',zIndex:'100',marginTop:'23%',left:'82%',color:'red'}} onClick={this.updateIcon.bind(this)}/>);
    }
        return(
      <div>
      <Grid>
        <Grid.Row style={{width:'100%'}}>
          <Grid.Column width={16}>
            <Link to='/mWomenCategory/LACE'>
            <Icon name='left arrow' size='large' style={{position:'absolute',zIndex:'100',marginTop:'3%',left:'5%',color:'black'}}/>
          </Link>
            <Link to='/infoPage'>
            <Icon circular name='info' size='large' style={{position:'absolute',zIndex:'100',marginTop:'3%',left:'82%',color:'black'}}/>
          </Link>
          <Link to='/signin'>
          {icon_component}
        </Link>
        <Link to='/ArView'>
        <Image src='https://thumbs.gfycat.com/WarmheartedSmartIrishterrier-max-1mb.gif' style={{height:'50px',borderRadius:'50%',top:'57%',position:'fixed',zIndex:'100',left:'75%'}}/>
        </Link>
        <Link to='/RecommendedAndCelebrities'>
        <Image src='https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSioLTyEZnxP00wULG3RELIFb_uZmyNS091XPSq77aGLcRNAMLE8g' style={{height:'50px',borderRadius:'50%',top:'65%',position:'fixed',zIndex:'100',left:'75%'}}/>
</Link>
          <Slider {...settings}>
            <div><img src='http://media.wallis.co.uk/wcsstore/Wallis/images/catalog/WL250991001_Large_M_1.jpg' style={{height:'75vh',width:'100%'}}/></div>
            <div><img src='http://media.wallis.co.uk/wcsstore/Wallis/images/catalog/WL250991001_Large_P_1.jpg' style={{height:'75vh',width:'100%'}}/></div>
            <div><img src='http://media.wallis.co.uk/wcsstore/Wallis/images/catalog/WL250991001_Large_F_1.jpg' style={{height:'75vh',width:'100%'}}/></div>
            <div><img src='http://media.wallis.co.uk/wcsstore/Wallis/images/catalog/WL250991001_Large_B_1.jpg' style={{height:'75vh',width:'100%'}}/></div>
            <div><img src='http://media.wallis.co.uk/wcsstore/Wallis/images/catalog/WL250991001_Large_D_1.jpg' style={{height:'75vh',width:'100%'}}/></div>

          </Slider>
          </Grid.Column>
        </Grid.Row>
        <Grid.Row/>
        <Grid.Row>
          <Grid.Column width={1}/>
          <Grid.Column width={10}>
            <h3 style={{fontSize:'18px',marginTop:'-10%'}}>Black Ombre Layered Top</h3>
          </Grid.Column>
          <Grid.Column width={5}/>
        </Grid.Row>
          <Grid.Row style={{marginTop:'-6%'}}>
            <Grid.Column width={1}/>
          <Grid.Column width={4} >
            <h2>$30.00</h2>
          </Grid.Column>
          <Grid.Column width={3}/>
          <Grid.Column width={7}>
            <Dropdown placeholder='Select Size' style={{fontSize:'18px'}} options={options}/>
          </Grid.Column>
          <Grid.Column width={1}/>
          </Grid.Row>
        <Grid.Row style={{marginTop:'-5%'}}>
          <Grid.Column width={1}/>
          <Grid.Column width={14}>
            <Link to="/signin">
          {button_component}
        </Link>
          </Grid.Column>
          <Grid.Column width={1}/>
        </Grid.Row>
        <Grid.Row>
          <Grid.Column width={1}/>
          <Grid.Column width={10}>
          <h4>INSPIRED BY LAST PURCHASE</h4>
          </Grid.Column>
          {/* <Grid.Column width={2}/> */}
          <Grid.Column width={4}>
          <span>5 Items</span>
          </Grid.Column>
        </Grid.Row>
      </Grid>
      <ProductRecommended/>
    </div>
  );
  }
}
