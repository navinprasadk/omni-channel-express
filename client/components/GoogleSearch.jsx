import React, {Component} from 'react';
import { Grid, Input, Card,  Image, Divider, Header, Icon, Dimmer, Loader } from 'semantic-ui-react';
import TrackVisibility from 'react-on-screen';
import {Link} from 'react-router-dom';
import Slider from 'react-slick';
import '../styles/style.css';

export default class GoogleSearch extends Component{
  constructor(props){
    super(props);
    this.state = {
      welcomeCardStatus: false,
      userInputCardStatus: false,
      adminInputCardStatus: false,
      textToDisplay: 'I',
      cardResults: '',
      cardResultStatus: false,
      googleImage: true,
      loaderStatus: false
    }
    this.handleCardResults = this.handleCardResults.bind(this);
  }
  componentDidMount() {
    {/*Timeout for welcomecard*/}
    setTimeout(() =>
    {
      this.setState({
        welcomeCardStatus: true
      });
    }, 500);
    {/*Timeout for userInputCard*/}
    setTimeout(() =>
    {
      let input = " want to buy a workwear women's Top";
      let data = input.split(" ");
      let i = 0;
      let timerId = setInterval(() =>
      {
        this.setState({
          userInputCardStatus: true
        })
        if(i < data.length) {
          this.setState({ textToDisplay: this.state.textToDisplay+' '+data[i]  });
        }
          i++;
          if(i === data.length+1) {
            clearInterval(timerId);
          }
      }, 500);
    }, 1000);
    {/*Timeout for adminInputCard*/}
    setTimeout(() =>
    {
      this.setState({
        adminInputCardStatus: true,
        loaderStatus: true
      });
    }, 5100);

    setTimeout(() =>
    {
        this.handleCardResults();
    }, 6000);
  }


  handleCardResults()
    {
      var temp,settings = {

         speed: 500,
         slidesToShow: 1,
         slidesToScroll: 1,
         centerMode: true
       };
       temp = (
         <Grid.Column width = {16} >
           <Slider {...settings} style={{
                bottom: '3%'
              }}>
                <div style={{
                    padding: '1rem',
                    maxWidth: '960px',
                    margin: '0 auto'
                }}>
                  <Card.Group>
                    <Card style={{
                        marginBottom: '5%'
                       }}
                       >
                      <Card.Content>
                        <Link to="/womenCategory">
                          <Card.Header id="googleSearchHeader" style={{
                              color: "#1967D2",
                              display: "block",
                              marginBottom: "-1px",
                              paddingTop: "1px",
                              lineHeight: "20px",
                              height: "40px",
                              overflow: "hidden",
                              textOverflow: "ellipsis",
                              fontSize: "16px",
                              fontWeight: "normal"
                            }}>
                            Women's Top: Buy Jeans Top Online at best prices in London - Aquaberry, one of the best online fashionable store in the world
                          </Card.Header>
                          <Card.Meta id="googleSearchLink" style={{
                              color: "#006621",
                              fontSize: "14px",
                              textOverflow: "ellipsis",
                              overflow: "hidden",
                              whiteSpace: "nowrap",
                              lineHeight: "20px"
                            }}>
                            https://m.aquaberry.com>Womens- Top>results
                          </Card.Meta>
                        </Link>
                        {/* </Card.Content> */}
                        <Divider></Divider>
                        <p id="googleSearchDescription" style={{
                            paddingTop: "1px",
                            marginBottom: "0%",
                            fontSize: "14px",
                            lineHeight: "20px",
                            wordWrap: "break-word",
                            textOverflow: "ellipsis"
                          }}>
                          Shop for women's tops. Available in a fabulous range from long tops to crop tops.
                        </p>
                      </Card.Content>
                    </Card>
                  </Card.Group>
                </div>
                <div style={{
                    padding: '1rem',
                    maxWidth: '960px',
                    margin: '0 auto'
                }}>
                  <Card.Group>
                    <Card style={{
                        marginBottom: '5%'
                      }}>
                      <Card.Content>
                        <Link to="/womenCategory">
                          <Card.Header id="googleSearchHeader" style={{
                              color: "#1967D2",
                              display: "block",
                              marginBottom: "-1px",
                              paddingTop: "1px",
                              lineHeight: "20px",
                              height: "40px",
                              overflow: "hidden",
                              textOverflow: "ellipsis",
                              fontSize: "16px",
                              fontWeight: "normal"
                            }}>
                            Women's Tops | Women's Shirts, Blouses, and T-Shirts | BOBO
                          </Card.Header>
                          <Card.Meta id="googleSearchLink" style={{
                              color: "#006621",
                              fontSize: "14px",
                              textOverflow: "ellipsis",
                              overflow: "hidden",
                              whiteSpace: "nowrap",
                              lineHeight: "20px"
                            }}>
                            https://m.bobo.com>Womens-topmaterials>womenstop
                          </Card.Meta>
                        </Link>
                        {/* </Card.Content> */}
                        <Divider></Divider>
                        <p id="googleSearchDescription" style={{
                            paddingTop: "1px",
                            marginBottom: "-1%",
                            fontSize: "14px",
                            lineHeight: "20px",
                            wordWrap: "break-word",
                            textOverflow: "ellipsis"
                          }}>
                          Free priority shipping · New arrivals daily · Customer Service Focused 
                        </p>
                      </Card.Content>
                    </Card>
                  </Card.Group>
                </div>
                <div style={{
                    padding: '1rem',
                    maxWidth: '960px',
                    margin: '0 auto'
                }}>
                  <Card.Group>
                    <Card style={{
                        marginBottom: '5%',
                        height:'10%',
                      }}>
                      <Card.Content>
                        <Link to="/womenCategory">
                          <Card.Header id="googleSearchHeader" style={{
                              color: "#1967D2",
                              display: "block",
                              marginBottom: "-1px",
                              paddingTop: "1px",
                              lineHeight: "20px",
                              height: "40px",
                              overflow: "hidden",
                              textOverflow: "ellipsis",
                              fontSize: "16px",
                              fontWeight: "normal"
                            }}>
                            Women's Shirts & Blouses | New Collection Online | Terra United States
                          </Card.Header>
                          <Card.Meta id="googleSearchLink" style={{
                              color: "#006621",
                              fontSize: "14px",
                              textOverflow: "ellipsis",
                              overflow: "hidden",
                              whiteSpace: "nowrap",
                              lineHeight: "20px"
                            }}>
                            https://m.terra.com>topforwomens-womens/women
                          </Card.Meta>
                        </Link>
                        {/* </Card.Content> */}
                        <Divider></Divider>
                        <p id="googleSearchDescription" style={{
                            paddingTop: "1px",
                            marginBottom: "-1%",
                            fontSize: "14px",
                            lineHeight: "20px",
                            wordWrap: "break-word",
                            textOverflow: "ellipsis"
                          }}>
                          Check out our gorgeous range of women's tops from Boohoo at an affordable price
                        </p>
                      </Card.Content>
                    </Card>
                  </Card.Group>
                </div>
            </Slider>
            </Grid.Column>
       )
       this.setState({
         cardResultStatus:true,
         cardResults:temp,
         googleImage: false,
         loaderStatus: false
       });
    }

render(){
  let welcomeCard = (
  <Grid.Column width = {16} >
    <div style={{
    display: 'block'
  }}>
  <Image src="http://www.iconninja.com/files/881/286/362/assistant-app-application-google-icon.svg" size="mini" alt="" style={{
      marginLeft: '2%'
    }}/> {/* <Image src="http://evananthony.com/myFiles/googleAssistant/opa_voice.gif" size="small" style={{ marginLeft:'2%' }}/> */}
  <Card style={{
      borderRadius: '30px',
      padding: '0 1%',
      margin: '-12% -2.5% 21% 12.5%',
      color: 'black',
      width: '50%'
    }}>
    <Card.Content>
      <Card.Description style={{
          color: '#212121'
        }}>Hi, how can I help you?</Card.Description>
    </Card.Content>
  </Card>
    </div>
    </Grid.Column>
      )
      let userInputCard = (
        <Grid.Column width={16} >
          <Card
            style={{
              borderRadius:'30px',
              float:'right',
              padding:'0 1%',
              margin:'-12% -2.5% 12% 15.5%',
              backgroundColor:'#e0e0e0',
              color:'black'
            }}>
            <Card.Content>
              <Card.Description style={{color:'#212121'}}>{this.state.textToDisplay}</Card.Description>
            </Card.Content>
          </Card>
        </Grid.Column>
      )
      let adminInputCard = (
        <Grid.Column width={16} >
          <div style={{ display:'block' }}>
            <Image src="http://www.iconninja.com/files/881/286/362/assistant-app-application-google-icon.svg" alt="" size="mini" style={{ marginLeft:'2%' }}/>
            <Card
              style={{
                borderRadius:'30px',
                padding:'0 1%',
                margin:'-12% -2.5% 12% 12.5%',
                color:'black',
                width:'40%'
              }}>
              <Card.Content>
                <Card.Description style={{color:'#212121'}}>Here you go</Card.Description>
              </Card.Content>
            </Card>
          </div>
        </Grid.Column>
      )
      return(
        <div style={{overflow:'hidden'}}> <Grid>
      {
        this.state.googleImage
          ? <Grid.Row style={{
                marginTop: "35%"
              }}>
              <Grid.Column width={2}></Grid.Column>
              <Grid.Column width={12}><Image src="https://upload.wikimedia.org/wikipedia/commons/thumb/2/2f/Google_2015_logo.svg/2000px-Google_2015_logo.svg.png" alt="" /></Grid.Column>
              <Grid.Column width={2}></Grid.Column>
            </Grid.Row>
          : null
      }

      <Grid.Row only='mobile' style={{
          position: 'absolute',
          bottom: '-5%'
        }}>
        {
          this.state.welcomeCardStatus
            ? welcomeCard
            : null
        }
        {
          this.state.userInputCardStatus
            ? userInputCard
            : null
        }
        {
          this.state.adminInputCardStatus
            ? adminInputCard
            : null
        }
        {
          this.state.cardResultStatus
            ? this.state.cardResults
            : null
        }
      </Grid.Row>
      {/* <Grid.Row only='mobile' style={{ position: 'fixed', bottom:'0%',marginTop: '5%', borderTop:'1px grey solid', padding: '0% 0% 0% 2%'}} >
                <Grid.Column width={1} style={{padding:'0 6%', marginTop: '3%'}}>
                  <Icon name="keyboard" size="big" style={{color:'grey'}}/>
                </Grid.Column>
                <Grid.Column width={14} style={{ textAlign: 'center' }}>
                  <Image src="https://3.bp.blogspot.com/-uuy4oHM8YNU/VelHUJMDJMI/AAAAAAACEzc/A0zvTppv8uE/s320/g-dots.gif" size="tiny"
                  style={{
                    marginLeft: '36%'
                  }}
                  />
                </Grid.Column>
                <Grid.Column width={1}></Grid.Column>
              </Grid.Row> */
      }
    </Grid>
    <Dimmer active={this.state.loaderStatus}>
      <Loader>Loading</Loader>
    </Dimmer>
    <TrackVisibility offset={1000}></TrackVisibility>
    </div>
  );
}
}
