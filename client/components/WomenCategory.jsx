import React, {Component} from 'react';
import { Grid, Segment, Image ,Input,Dropdown,Header,Icon,Button,Divider,Menu,Label} from 'semantic-ui-react';
import Drawer from 'material-ui/Drawer';
import Appbar from './AppBar.jsx';
import {Link} from 'react-router-dom';
import '../styles/style.css';


export default class WomenCategory extends Component{
  constructor(props){
    super(props);
    this.state={
      text:"what's New",
      open: false,
      open1:false,
      secondaryText1:'All',
      secondaryText2:'All',
      secondaryText3:['All'],
      secondaryText4:'All',
      secondaryText5:'All',
      secondaryText6:'All',
      brandValue:[]
    }
  }
  handleToggle() {
    this.setState({
      open: !this.state.open
    });
  }
  handleClose() {
    this.setState({open: false});
  }
  handleBrand() {
    this.setState({
      open1: !this.state.open1
    });
  }
  handleChange(e,value){
    this.setState({secondaryText3:value.value})
  }
render(){
  const brandOptions = [
  { key: 'adidas', text: 'Adidas', value: 'adidas' },
  { key: 'nike', text: 'Nike', value: 'nike' }
]
//console.log("--->",this.state.secondaryText3);
  return(
    <div>

      <Menu secondary style={{marginBottom:'1%',position:'fixed',backgroundColor:'#212121',width:'100%',color:'white',zIndex:'100'}}>
        <Link to='/mhome'>
        <Menu.Item style={{marginTop:'18%'}}>
          <Icon name='arrow left' position='left' size='large' style={{color:'white'}}/>
        </Menu.Item>
        </Link>
        <h3 style={{letterSpacing:'3px'}}>{this.props.match.params.category}</h3>
        <Menu.Item>
        </Menu.Item>
        <Menu.Menu position='right'>
          <Menu.Item>
            <Icon name='heart' size='large' style={{color:'white'}}/>
          </Menu.Item>
          <Menu.Item>
            <Icon name='cart' size='large' style={{color:'white'}}/>
          </Menu.Item>
          <Menu.Item>
            <Icon name='search' size='large' style={{color:'white'}}/>
          </Menu.Item>
        </Menu.Menu>
      </Menu>
      <Divider/>
    <Grid style={{fontWeight:'bold',fontSize:'15px'}}>
      <Grid.Row/>
      <Grid.Row/>
      <Grid.Row>
        <Grid.Column width={1} />
        <Grid.Column width={7} style={{position:'fixed',zIndex:'100',left:'2%'}}>
          <Dropdown text='SORT'>
           <Dropdown.Menu>
             <Dropdown.Item text={this.state.text} />
             <Dropdown.Item text='Price - Low to High' />
             <Dropdown.Item text='Price - High to Low' />
           </Dropdown.Menu>
         </Dropdown>
        </Grid.Column>
        <Grid.Column width={7} style={{position:'fixed',zIndex:'100',left:'50%'}}>
          <h4 style={{textAlign:'right'}} onClick={this.handleToggle.bind(this)}>FILTER</h4>
        </Grid.Column>
        <Grid.Column width={1} />
      </Grid.Row>
      <Divider style={{marginTop:'3%'}}/>
      <Grid.Row style={{marginTop:'-5%',color:'gray'}}>
        <Grid.Column width={1}/>
        <Grid.Column width={14}>
          <p style={{textAlign:'center'}}>250 items found</p>
        </Grid.Column>
        <Grid.Column width={1}/>
      </Grid.Row>
      <Image src='https://thumbs.gfycat.com/WarmheartedSmartIrishterrier-max-1mb.gif' style={{height:'50px',borderRadius:'50%',top:'17%',position:'fixed',zIndex:'100',left:'75%'}}/>

      <Grid.Row>
        <Grid.Column width={1}/>
        <Grid.Column width={7}>
          <Link to='/productDetails/top5'>
          <Image src='./client/assets/Images/top5.jpg' alt="" />
          </Link>
          <Link to="/signin">
          <Icon name='empty heart' style={{position: 'absolute',top: '8px',right: '16px'}}/></Link>
          <div>
            <center>Black Ombre Layered</center>
            <center>$30.00</center>
          </div>
        </Grid.Column>

        <Grid.Column width={7}>
          <Link to='/productDetails/top1'>
          <Image src='./client/assets/Images/top1.jpg' alt="" />
          </Link>
          <Link to="/signin">

          <Icon name='empty heart' style={{position: 'absolute',top: '8px',right: '16px'}}/>
        </Link>
          <div>
            <center>Black Ombre Layered</center>
            <center>$30.00</center>
          </div>
        </Grid.Column>
        <Grid.Column width={1}/>
      </Grid.Row>
    <Grid.Row>
        <Grid.Column width={1}/>
        <Grid.Column width={7}>
          <Image src='./client/assets/Images/top2.jpg' alt="" />
          <Link to="/signin">

          <Icon name='empty heart' style={{position: 'absolute',top: '8px',right: '16px'}}/>
        </Link>
          <div>
            <center>Black Ombre Layered</center>
            <center>$30.00</center>
          </div>
        </Grid.Column>
        <Grid.Column width={7}>
          <Image src='./client/assets/Images/top3.jpg' alt="" />
          <Link to="/signin">

          <Icon name='empty heart' style={{position: 'absolute',top: '8px',right: '16px'}}/>
        </Link>
          <div>
            <center>Black Ombre Layered</center>
            <center>$30.00</center>
          </div>
        </Grid.Column>
        <Grid.Column width={1}/>
      </Grid.Row>
      <Grid.Row>
        <Grid.Column width={1}/>
        <Grid.Column width={7}>
          <Image src='./client/assets/Images/top6.jpg' alt="" />
          <Link to="/signin">

          <Icon name='empty heart' style={{position: 'absolute',top: '8px',right: '16px'}}/>
        </Link>
          <div>
            <center>Black Ombre Layered</center>
            <center>$30.00</center>
          </div>
        </Grid.Column>
        <Grid.Column width={7}>
          <Image src='./client/assets/Images/top5.jpg' alt="" />
          <Link to="/signin">

          <Icon name='empty heart' style={{position: 'absolute',top: '8px',right: '16px'}}/>
        </Link>
          <div>
            <center>Black Ombre Layered</center>
            <center>$30.00</center>
          </div>
        </Grid.Column>
        <Grid.Column width={1}/>
      </Grid.Row>
    </Grid>
    <Drawer docked={false} width={350} openSecondary={true} open={this.state.open} onRequestChange={(open) => this.setState({open})}>
      <Grid>
        <Grid.Row>
          <Grid.Column width={1} />
          <Grid.Column width={13}>
            <h3 style={{marginTop:'12%',fontWeight:'bold'}}>FILTER</h3>
            <Menu vertical size='large'>
              <Menu.Item name='Product Type' >
                <Label as='a' basic color='blue'>{this.state.secondaryText1}</Label>
                  Product Type
                </Menu.Item>
                <Menu.Item name='Style'>
                  <Label as='a' basic color='blue'>{this.state.secondaryText2}</Label>
                    Style
                  </Menu.Item>
                 <Menu.Item name='Brand' onClick={this.handleBrand.bind(this)}>
                   {this.state.secondaryText3.map((item,key)=>{
                     return(<Label as='a' basic color='blue' key={key}>{item}</Label>)
                   })
                 }
                     Brand
                   </Menu.Item>
                 <Menu.Item name='Range'>
                   <Label as='a' basic color='blue'>{this.state.secondaryText4}</Label>
                     Range
                   </Menu.Item>
                 <Menu.Item name='Color'>
                   <Label as='a' basic color='blue'>{this.state.secondaryText5}</Label>
                     Color
                   </Menu.Item>
                 <Menu.Item name='Size'>
                   <Label as='a' basic color='blue'>{this.state.secondaryText6}</Label>
                     Size
                   </Menu.Item>
           </Menu>
             <br/>
             <Button fluid secondary onClick={this.handleClose.bind(this)}> Done </Button>
     </Grid.Column>
     <Grid.Column width={2} />
     </Grid.Row>
     </Grid>
    </Drawer>
    <Drawer docked={false} width={350} openSecondary={true} open={this.state.open1} onRequestChange={(open1) => this.setState({open1})}>
      <Grid>
        <Grid.Row>
          <Grid.Column width={1} />
          <Grid.Column width={11}>
            <h3 style={{marginTop:'12%',fontWeight:'bold'}}>Brand</h3>
            <Dropdown placeholder='Select Brands' fluid multiple selection options={brandOptions} onChange={this.handleChange.bind(this)} />
         </Grid.Column>
         <Grid.Column width={2} />
         </Grid.Row>
         </Grid>
        </Drawer>

  </div>
  );
}
}
