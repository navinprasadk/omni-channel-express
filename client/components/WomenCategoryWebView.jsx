import React, {Component} from 'react';
import { Grid, Segment, Image, Modal, Header, Icon, Button, Container, List, Loader, Dimmer } from 'semantic-ui-react';
import AppBarWebView from './AppBarWebView.jsx';
import '../styles/style.css';

export default class WomenCategoryWebView extends Component{
  constructor(props){
    super(props);
    this.state={
      downloadMessage:false,
      loaderStatus: true
    }
  }
  // componentDidMount() {
  //   setTimeout(()=>{
  //     this.setState({ downloadMessage:true      });
  //   }, 3000);
  //   }

  componentWillMount() {
      this.setState({
        loaderStatus: true
      });
  }

  componentDidMount() {
    setTimeout(() =>
    {
      this.setState({
        loaderStatus: false
      });
    }, 2000);
  }



render(){
    return(
      <div>
          {
            this.state.loaderStatus ?
              <Grid style={{
                fontWeight: 'bold',
                fontSize: '15px'
              }}>
              <Dimmer active>
                <Loader>Loading...</Loader>
              </Dimmer>
              </Grid> :
            (
              <Grid style={{
                fontWeight: 'bold',
                fontSize: '15px'
              }}>
                <Grid.Row style={{
                    height: "56"
                  }}>
                  <Grid.Column width={16}><AppBarWebView/></Grid.Column>
                </Grid.Row>

                <Grid.Row>
                  <Grid.Column width={16}>
                    <center style={{textTransform:"UPPERCASE", fontWeight:"bold",  letterSpacing:"1px"}}>Women's Tops</center>
                  </Grid.Column>
                </Grid.Row>

                <Grid.Row>
                  <Grid.Column width={8}><center style={{textTransform:"Capitalise", fontWeight:"bold",  letterSpacing:"1px"}}>SORT</center></Grid.Column>
                  <Grid.Column width={8}><center style={{textTransform:"Capitalise", fontWeight:"bold",  letterSpacing:"1px"}}>FILTER</center></Grid.Column>
                </Grid.Row>

                <Grid.Row>
                  <Grid.Column width={16}>
                    <center style={{color:"#9e9e9e", letterSpacing:"1px"}}>12,583 items found</center>
                  </Grid.Column>
                </Grid.Row>

                <Grid.Row  only="mobile">
                  <Grid.Column width={1}/>
                  <Grid.Column width={7}>
                    <Image src='./client/assets/Images/NavyStripedBlouse.jpg' alt="" />
                    <div>
                      <center>
                        <p className="WomenCategoryWebViewTitle">Navy striped Blouse</p>
                      </center>
                      <center>
                        <p className="WomenCategoryWebViewPrice">$40.00</p>
                      </center>
                    </div>
                  </Grid.Column>
                  <Grid.Column width={7}>
                    <Image src='./client/assets/Images/BlackMeshTop.jpg' alt="" />
                    <div>
                      <center>
                        <p className="WomenCategoryWebViewTitle">Black Mesh Top</p>
                      </center>
                      <center>
                        <p className="WomenCategoryWebViewPrice">$40.00</p>
                      </center>
                    </div>
                  </Grid.Column>
                  <Grid.Column width={1}/>
                </Grid.Row>

                <Grid.Row  only="mobile">
                  <Grid.Column width={1}/>
                  <Grid.Column width={7}>
                    <Image src='./client/assets/Images/NavySleeveTop.jpg' alt="" />
                    <div>
                      <center>
                        <p className="WomenCategoryWebViewTitle">Navy Gingham Balloon Sleeve Top</p>
                      </center>
                      <center>
                        <p className="WomenCategoryWebViewPrice">$30.00</p>
                      </center>
                    </div>
                  </Grid.Column>
                  <Grid.Column width={7}>
                    <Image src='./client/assets/Images/BerryMicroRuffleTop.jpg' alt="" />
                    <div>
                      <center>
                        <p className="WomenCategoryWebViewTitle">Berry Micro Ruffle Top</p>
                      </center>
                      <center>
                        <p className="WomenCategoryWebViewPrice">$20.00</p>
                      </center>
                    </div>
                  </Grid.Column>
                  <Grid.Column width={1}/>
                </Grid.Row>

                <Grid.Row  only="mobile">
                  <Grid.Column width={1}/>
                  <Grid.Column width={7}>
                    <Image src='./client/assets/Images/NavyFloralTop.jpg' alt="" />
                    <div>
                      <center>
                        <p className="WomenCategoryWebViewTitle">Navy Floral Print Frill Shell Top</p>
                      </center>
                      <center>
                        <p className="WomenCategoryWebViewPrice">$30.00</p>
                      </center>
                    </div>
                  </Grid.Column>
                  <Grid.Column width={7}>
                    <Image src='./client/assets/Images/BlackTunicTop.jpg' alt="" />
                    <div>
                      <center>
                        <p className="WomenCategoryWebViewTitle">Black Paisley Print Swing Tunic Top</p>
                      </center>
                      <center>
                        <p className="WomenCategoryWebViewPrice">$20.00</p>
                      </center>
                    </div>
                  </Grid.Column>
                  <Grid.Column width={1}/>
                </Grid.Row>

                <Grid.Row  only="mobile">
                  <Grid.Column width={1}/>
                  <Grid.Column width={7}>
                    <Image src='./client/assets/Images/SilverTwistTop.jpg' alt="" />
                    <div>
                      <center>
                        <p className="WomenCategoryWebViewTitle">Silver Twist Neck Top</p>
                      </center>
                      <center>
                        <p className="WomenCategoryWebViewPrice">$35.00</p>
                      </center>
                    </div>
                  </Grid.Column>
                  <Grid.Column width={7}>
                    <Image src='./client/assets/Images/NavyPrintedShirt.jpg' alt="" />
                    <div>
                      <center>
                        <p className="WomenCategoryWebViewTitle">Navy printed Zip Shirt</p>
                      </center>
                      <center>
                        <p className="WomenCategoryWebViewPrice">$40.00</p>
                      </center>
                    </div>
                  </Grid.Column>
                  <Grid.Column width={1}/>
                </Grid.Row>

                <Grid.Row>
                  <Grid.Column width={1}/>
                  <Grid.Column width={7}>
                    <Image src='./client/assets/Images/GreyJumper.jpg' alt="" />
                    <div>
                      <center>
                        <p className="WomenCategoryWebViewTitle">Grey Textured V-Neck Tunic Jumper</p>
                      </center>
                      <center>
                        <p className="WomenCategoryWebViewPrice">$20.00</p>
                      </center>
                    </div>
                  </Grid.Column>
                  <Grid.Column width={7}>
                    <Image src='./client/assets/Images/BlueJumper.jpg' alt="" />
                    <div>
                      <center>
                        <p className="WomenCategoryWebViewTitle">Blue Button Detail Jumper</p>
                      </center>
                      <center>
                        <p className="WomenCategoryWebViewPrice">$40.00</p>
                      </center>
                    </div>
                  </Grid.Column>
                  <Grid.Column width={1}/>
                </Grid.Row>

              </Grid>
            )
          }
      </div>
    );
  }
}
