import React, {Component} from 'react';
import { Grid, Image } from 'semantic-ui-react';
import {Link} from 'react-router-dom';
import AppBar from './AppBar.jsx';
import '../styles/style.css';

export default class PageNotFound extends Component{
  constructor(props){
    super(props);
    this.state={
      image:(
        <Image src='https://d1vs5fqeka2glf.cloudfront.net/59/56/599ef392625a05d4fe9ab5979ae96656_200x200.jpg' />
      )
    }
  }
  clickimage1(){
    this.setState({image:(<Image src='https://d1vs5fqeka2glf.cloudfront.net/52/ee/5259f49ba51b1a6649d84f00cb99a7ee_200x200.jpg'/>)})
  }
  clickimage2(){
    this.setState({image:(<Image src='https://d1vs5fqeka2glf.cloudfront.net/86/aa/8696f1ab66d3cf2054153eb38ad9e3aa_200x200.jpg'/>)})
  }
  clickimages3(){
    this.setState({image:(<Image src='https://d1vs5fqeka2glf.cloudfront.net/02/60/028f961371f81e78571c1df5d1a34b60_200x200.jpg'/>)})
  }
  clickimages4(){
    this.setState({image:(<Image src='https://d1vs5fqeka2glf.cloudfront.net/95/4c/95110cee20838f31e4a0cd12982a364c_200x200.jpg'/>)})
  }
render(){
  return(
    <div style={{overflow:'hidden'}}>
      <Grid>
        <Grid.Row>
          <Grid.Column width={16}>
<AppBar/>
          </Grid.Column>

        </Grid.Row>
      </Grid>
    <Grid>

      <Grid.Row>
      </Grid.Row>
      <Grid.Row verticalAlign='top'>
      <Grid.Column width={7}>
        <Image src='https://d1vs5fqeka2glf.cloudfront.net/52/ee/5259f49ba51b1a6649d84f00cb99a7ee_200x200.jpg' onClick={this.clickimage1.bind(this)}/>
      </Grid.Column>
      <Grid.Column>
      </Grid.Column>
      <Grid.Column width={7}>
        <Image src='https://d1vs5fqeka2glf.cloudfront.net/86/aa/8696f1ab66d3cf2054153eb38ad9e3aa_200x200.jpg' onClick={this.clickimage2.bind(this)} />
      </Grid.Column>
    </Grid.Row>
    <Grid.Row verticalAlign='middle'>
      <Grid.Column width={4}>

      </Grid.Column>
      <Grid.Column width={8}>
        {this.state.image}
      </Grid.Column>
      <Grid.Column width={4}>

      </Grid.Column>
    </Grid.Row>
    <Grid.Row verticalAlign='bottom'>
    <Grid.Column width={7}>
      <Image src='https://d1vs5fqeka2glf.cloudfront.net/02/60/028f961371f81e78571c1df5d1a34b60_200x200.jpg' onClick={this.clickimages3.bind(this)} />
    </Grid.Column>
    <Grid.Column>
      <br />
    </Grid.Column>
    <Grid.Column width={7}>
      <Image src='https://d1vs5fqeka2glf.cloudfront.net/95/4c/95110cee20838f31e4a0cd12982a364c_200x200.jpg' onClick={this.clickimages4.bind(this)}/>
    </Grid.Column>
  </Grid.Row>
    </Grid>
    </div>
  );
}
}
