import React, {Component} from 'react';
import {Menu, Icon, Grid, Image, Header} from 'semantic-ui-react';
import Drawer from 'material-ui/Drawer';
import MenuItem from 'material-ui/MenuItem';
var Carousel = require('nuka-carousel');
import ProductDetails from './ProductDetails.jsx';
export default class AppBar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false
    }
  }
  handleToggle() {
    this.setState({
      open: !this.state.open
    });
  }
  handleClose() {
    this.setState({open: false});
  }
  render() {
    console.log('........',this.state.cart);
    return (
      <div>
      <Menu secondary style={{position:'fixed',zIndex:'100'}}>
         <Menu.Item>
           <Icon name="bars" size="large" onClick={this.handleToggle.bind(this)}/>
         </Menu.Item>
         <Menu.Item >
           <span className="brand" style={{fontSize:'28px', marginLeft:'-3%'}}>Aquaberry</span>
         </Menu.Item>
         <Menu.Menu position='right'>
           <Menu.Item>
             <Icon name="shopping bag" size="large"/>
           </Menu.Item>
           <Menu.Item>
             <Icon name="search" size="large"/>
           </Menu.Item>
         </Menu.Menu>
       </Menu>
       <Drawer docked={false} width={250} open={this.state.open} onRequestChange={(open) => this.setState({open})}>

         <Menu.Item></Menu.Item>
         <Menu.Item></Menu.Item>

        <MenuItem style={{marginTop:'10%'}}>
          <Header as='h4' style={{letterSpacing:"2px", textTransform:"capitalize"}}><Icon size='small' name='home' style={{color:'#212121'}}/>Home</Header>
        </MenuItem>

        <MenuItem>
          <Header as='h4' style={{letterSpacing:"2px", textTransform:"capitalize"}}><Icon size='small' name='shopping bag' style={{color:'#212121'}}/>Bag</Header>
        </MenuItem>

        <MenuItem>
          <Header as='h4' style={{letterSpacing:"2px", textTransform:"capitalize"}}><Icon size='small' name='heart' style={{color:'#212121'}}/>Saved Items</Header>
        </MenuItem>

        <MenuItem>
          <Header as='h4' style={{letterSpacing:"2px", textTransform:"capitalize"}}><Icon size='small' name='home' style={{color:'#212121'}}/>My Account</Header>
        </MenuItem>

        <MenuItem>
          <Header as='h4' style={{letterSpacing:"2px", textTransform:"capitalize"}}><Icon size='small' name='setting' style={{color:'#212121'}}/>Settings</Header>
        </MenuItem>

        <MenuItem>
          <Header as='h4' style={{letterSpacing:"2px", textTransform:"capitalize"}}><Icon size="small" name='info' style={{color:'#212121'}}/>Help & FAQ's</Header>
        </MenuItem>
      </Drawer>

      </div>
      );
  }
}
