import React, {Component} from 'react';
import {Menu, Icon, Grid, Image, Header} from 'semantic-ui-react';
import {Link} from 'react-router-dom';
import Drawer from 'material-ui/Drawer';
import MenuItem from 'material-ui/MenuItem';
var Carousel = require('nuka-carousel');
import ProductDetails from './ProductDetails.jsx';
export default class AppBar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false
    }
  }
  handleToggle() {
    this.setState({
      open: !this.state.open
    });
  }
  handleClose() {
    this.setState({open: false});
  }
  render() {
    console.log('........',this.state.cart);
    return (
      <div>
      <Menu secondary style={{position:'fixed',zIndex:'100',width:'102%',backgroundColor:'#212121'}}>
         <Menu.Item style={{color:'#EEEEEE'}}>
           <Icon name="bars" size="large" onClick={this.handleToggle.bind(this)}/>
         </Menu.Item>
         <Link to='/mHome'>
         <Menu.Item>
           <span className="brand" style={{fontSize:'28px', marginLeft:'13%',fontFamily:'Lily Script One,cursive',color:'#EEEEEE'}}>Aquaberry</span>
         </Menu.Item>
         </Link>
         <Menu.Menu position='right'>
           <Menu.Item>
             <Icon name="shopping bag" size="large" style={{color:'#EEEEEE'}}/>
           </Menu.Item>
           <Menu.Item>
             <Icon name="search" size="large" style={{marginLeft:'2%',color:'#EEEEEE'}}/>
           </Menu.Item>
         </Menu.Menu>
       </Menu>
       <Drawer docked={false} width={250} open={this.state.open} onRequestChange={(open) => this.setState({open})}>

         <Menu.Item></Menu.Item>
         <Menu.Item></Menu.Item>

        <MenuItem style={{marginTop:'10%'}}>
          <Header as='h4' style={{letterSpacing:"2px", textTransform:"capitalize"}}><Icon size='small' name='home' style={{color:'#212121'}}/>Home</Header>
        </MenuItem>

        <MenuItem>
          <Header as='h4' style={{letterSpacing:"2px", textTransform:"capitalize"}}><Icon size='small' name='shopping bag' style={{color:'#212121'}}/>Bag</Header>
        </MenuItem>

        <MenuItem>
          <Header as='h4' style={{letterSpacing:"2px", textTransform:"capitalize"}}><Icon size='small' name='heart' style={{color:'#212121'}}/>Saved Items</Header>
        </MenuItem>

        <MenuItem>
          <Header as='h4' style={{letterSpacing:"2px", textTransform:"capitalize"}}><Icon size='small' name='home' style={{color:'#212121'}}/>My Account</Header>
        </MenuItem>

        <MenuItem>
          <Header as='h4' style={{letterSpacing:"2px", textTransform:"capitalize"}}><Icon size='small' name='setting' style={{color:'#212121'}}/>Settings</Header>
        </MenuItem>

        <MenuItem>
          <Header as='h4' style={{letterSpacing:"2px", textTransform:"capitalize"}}><Icon size="small" name='info' style={{color:'#212121'}}/>Help & FAQ's</Header>
        </MenuItem>
      </Drawer>

      </div>
      );
  }
}
