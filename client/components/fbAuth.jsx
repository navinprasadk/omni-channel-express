import React, {Component} from 'react';
import { Grid, Menu, Icon, Button } from 'semantic-ui-react';
import {Link} from 'react-router-dom';
import '../styles/style.css';

export default class fbAuth extends Component{
  constructor(props){
    super(props);
  }
render(){
  return(
    <div className="fbAuth_Background" style={{backgroundColor:"#E9EBEE"}}>
    <Grid className="fbFont">
      <Grid.Row only='mobile' style={{ paddingBottom:"5%"}}>
        <Grid.Column width={16}>
          <Menu secondary style={{backgroundColor:'#3b5998', color:'#eee'}}>
              <Menu.Item style={{color:'white'}}><Icon size="large" name="facebook"/></Menu.Item>
              <Menu.Item style={{fontWeight:'normal', letterSpacing:'1px', textTransform:'capitalize', color:"white", fontSize:'140%'}} name='post to facebook' />
          </Menu>
        </Grid.Column>
      </Grid.Row>

      <Grid.Row only='mobile' style={{marginTop:"20%"}}>
        <Grid.Column width={2}></Grid.Column>
        <Grid.Column width={12}><center><Icon size="huge" name="edit"/></center></Grid.Column>
        <Grid.Column width={2}></Grid.Column>
      </Grid.Row>

      <Grid.Row only='mobile'>
        <Grid.Column width={2}></Grid.Column>
        <Grid.Column width={12} style={{letterSpacing:"1px", fontSize:"110%"}} ><center><p><span style={{fontWeight:"bold"}}>Aquaberry</span> would like to post publicly on Facebook for you. Who can see Aquaberry's post on your Timeline?</p></center></Grid.Column>
        <Grid.Column width={2}></Grid.Column>
      </Grid.Row>

      <Grid.Row only='mobile'>
        <Grid.Column width={2}></Grid.Column>
        <Grid.Column width={12}> <center> <Button size="huge" as='div' labelPosition='right'>
      <Button content='standard' basic>
        <Icon name='globe' />
        Public
      </Button>
    </Button></center></Grid.Column>
        <Grid.Column width={2}></Grid.Column>
      </Grid.Row>

      <Grid.Row only='mobile' style={{marginTop:"10%"}}>
        <Grid.Column width={2}></Grid.Column>
        <Grid.Column width={6}> <center><Button fluid  content="Not Now" basic style={{letterSpacing:"1px"}}></Button> </center></Grid.Column>
        {/* <Grid.Column width={}></Grid.Column> */}
        <Grid.Column width={6}> <center> <Button fluid content="OK" style={{backgroundColor:"#4D89FF", color:"white", letterSpacing:"1px"}}></Button> </center></Grid.Column>
        <Grid.Column width={2}></Grid.Column>

      </Grid.Row>



    </Grid>
  </div>
  );
}
}
