import React from 'react';
import { Menu,Icon,Segment,Input,Grid,Image,Button,List, Dimmer, Loader } from 'semantic-ui-react';
import '../styles/style.css';
import AppBar from './AppBar.jsx';
import {Link} from 'react-router-dom';

export default class MobileAppHome extends React.Component {
  constructor(props){
    super(props);
    this.state={
      mAppLoaderStatus:true
    }
  }
  componentDidMount(){
    setTimeout(()=>{
      this.setState({
        mAppLoaderStatus:false
      })
    },2000);
  }
   render() {
      return (
         <div style={{overflow:'hidden'}}>
           {
             this.state.mAppLoaderStatus ?
             <Grid style={{fontWeight: 'bold'}}>
               <Dimmer active><Loader>Loading...</Loader></Dimmer>
             </Grid> :
           (
             <div>
               <Grid>
                 <Grid.Row>
                   <Grid.Column width={16}>
<AppBar/>
                   </Grid.Column>

                 </Grid.Row>
               </Grid>

         <Image src='https://thumbs.gfycat.com/WarmheartedSmartIrishterrier-max-1mb.gif' style={{height:'50px',borderRadius:'50%',top:'10%',position:'fixed',zIndex:'100',left:'85%'}}/>
          <Grid style={{marginTop:'38px'}}>
            <Grid.Row>
              <Grid.Column width={16}>
                <Image src="./client/assets/Images/hero.jpg" alt="" />
                <span style={{position:'absolute',top:'30%',left:'20%',color:'white',fontSize:'35px'}} className="heroText">All Dressed Up</span>
                <span style={{position:'absolute',top:'45%',left:'29%',color:'white',fontSize:'15px',fontWeight:'bold'}} className="heroText">DRESSES FROM $30</span>
                <Button inverted style={{position:'absolute',top:'59%',left:'34%'}}>Shop Now</Button>
              </Grid.Column>
            </Grid.Row>

            <Grid.Row style={{marginTop:'-10px'}}>
              <Grid.Column width={16}>
                <center><span className="brandCategories" style={{fontSize:'20px',fontWeight:'bold'}}>On Trend</span></center>
              </Grid.Column>
            </Grid.Row>

            <Grid.Row style={{marginLeft:'10px',marginRight:'10px'}}>
              <Grid.Column width={8}>
                <Image src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxAPDw8PDQ8PDw8ODg4PDw4PDw8NDw0PFREXFhUVFRUYHSggGBolGxUXITEhJSkrLi4wFx8zODMsNygtLisBCgoKDg0OGhAQGi0lHSYrLS0tLS0tLS0tLS0vLS0rLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLf/AABEIAQ4AugMBEQACEQEDEQH/xAAcAAACAgMBAQAAAAAAAAAAAAAAAQUGAgMEBwj/xABCEAACAQIDAwkEBwYFBQAAAAAAAQIDEQQSIQUxUQYTIkFhcYGRoTJSscEHM0JicrLRFCM0guHwFVNzovFjkpOzwv/EABoBAQADAQEBAAAAAAAAAAAAAAABBAUCAwb/xAAuEQEAAgIBAwMDAwMFAQAAAAAAAQIDESEEEjEiMkFRcYEzQmETsfAUIySRwQX/2gAMAwEAAhEDEQA/APYkQGAwABoAAYDAYABGbe25RwVPPVervkgvam/ku04veKvTHjm8vPtpcs5VM0pSdlrbNlp0l2pPf5srTe1vK3GOtfCJw30mwjOPN1K8mpJWcIc3NX17UvE9NWjl5brPD2DZeOjXpxqQekktHvT4M9623G1e1ZidS7CUABAAAAgggEEhgIDBBBgMAAYAAwGAwMalRRi5SdlFNt8EldiZ0mI3OngvKrlBPG4ycovoptU7+zGMevu/vrKnunuleiO2O2GOxtj/ALdVjhrzdJRc681va7+1v1Od8u+2NalYcf8AR/hadp0YZZxyuzblFtcV2nNsloTXFSRyR5ZzoY54bF5aaqVFCSu1GM/syj1W3ea4Htitrn4eGam+Pl68mWVQAAAAgggAJIBBAAwAYAAwGAAMBgBIrvL/ABjpYCtldpVbUk1vSk9X5HjmnVXtgru7wOF3JqKvJuyS9F/fErzK5WHovIXEfs0VSlh3CdTWdbnKdRSfUui9F/U4n0zvb0iJtGpjSR5TbfqUZOFNUIJJZq2Im1CPXZRWrdu4iI7pJjtjagctI85Sw+OTpTlOo6EpUlKMZvfHR3cWmpLez1pXzDxyz4s9q5CY6WI2dhas5ZpypJSfbF217dCzj9qpljVpT528yCAAgABBIAQQQGIDAYAAwABgMAA8w+mDlAo5MDT9tx52rL3Iv2V2Pr8uJXzTuYhawV1Hc865J4OGMxEqcpSjTjTa6LyyblpmPHJukbWsWrzMfR6VgOSuHwcHKlmlJtO7fXp1LuXrxOMl5tXl1irFbcJLFbLoYiU1Xpxnezd7O259Xak/BEVmYlNoiYiEftLZFCUKWHhTioQrxrW1WsbuTvvvrv4sd08nbHCQ+i3HqpDGUk9KWLqOLvfNGUpWl4qJcw8RpR6nmdryeysAABAIACSCAAgMQGAwABgADQAAwPn/AJeVI1pV6z1q161Spm16NCM3SpRXY1CT8in3bttoRXVdK/yOxUaOOhHpRnV6KemR9G6XfdE5azNNmC0Vya+r1/EbSgsNJ1YuUbJSioym5Pdlyrf3FaN/C3Fdy1YPaadRRhg6lJVI25x0nSWi0TzJPclY7tEnZERtTvpD25Wo1MPQw9aVKVaNV1XC2bm3ZRV98btS1Vnod4aRMTMq2fJMTEQs30OVFCOJg96dN3+6k1/fce2OfU8M0emHqSLCoAEAgABAACAAEAAMAAYAAwACL5QY2VOlzdL+IxF6VBb7Sa6VRr3YK8n3dpxktqNR5l6Y67nc+IeD8tKsVia1Gn7FK1CN9+WmlBfC/e2VYjlc3wpuMnJSg4NxlGfQa3pqWj8yxTwr33uJh67ye25GcYwxHRm0lLqjN8exlGeJ4aUbnn5TmM2phaUbweabW5Nyb89xEynVp8vFtqY+eKxletW385KMUt0IwbUYrut6suR6axpnz6rzMvRvoexEv2ionrGUGn4K5zT3u8v6b2iG5X4FpRMAAQAAgABAACAAGAEhgBAYABFOOHoVXKrUUsTWi1ebvPItcsF9mPYuF9TmKfPy6teZjXw8G+kDBVqGNq1HTcqWJqSlQlC81O7zZLLVS7OvqvrbznFMPeM0TCC2XyfxdfE0IywuJjTU4ynOdCrCKSeZttrrtYm0TWszpFLRa8Rt6lU2JGWWUOjJMzdtTbV/g03NZtyDrcPNeVmx6uDxNaUoPma05Tp1PsLO7tN9Tu2XcdovWI+YZ+SvZaZ+JelfQVgMyxGIl1ZIRW/tfyOscbvLjNOqRD2AsqhEAAQCAAEAAIAAAGAEhgBAYGNWooRcpOyXq+C7R4TETPhUZ4WpWxDq1VkjG8orLGU5y3Xbd8sbOySs9931KKZe6ZiI4dZMXZWJmeZacRRxE6icFDmotq8ZyVX2cr6Lsmr/AHk9PA9ni5P8RxdKolKkqqedczTw+JU3p0Wq87U733xdlr7WmsSmNOrCTcnmr5Y1Jazik0oP3bvfZaX6zKyTE3mZa+OvbSIq6as6cWrJyfCKuczNYdRW0uWrQ5xNOk3F++o5Wu051vw63rzKR5NYWng4TjRo06anLPKNNWi5Wte3gWMV5rHKrmpFp4Tn7XJ2yqHi9T2/q2+Hh/Sr87b6ddOybSlw49x6VvE8fLztjmOfhtO3mQCAAEAgAAAAGSABgBAYFV2/jZSxPNRbUaaV+2TV2yh1N57u1o9LSIp3S6qGkLt71d9xcwV1SP5Ueot3X+woRaiuL1fe9X8Sw8W0DGpRUlqjyyYq38vXHltSeGhUubu7eKKVsM412uaMnG2qVe7PGbvbtduGmrHpW3DztBV5IiZKwitqYqUI5ot3TTXYeVrTHMPbHWJ4lcKU80Yy96MX5q5qxO4ZMxqdMmEEAgABAAAAAMAAaAYABRMXUz4qu111HFeDsjNzc5Zhq4eMMJ1x6KiuyPh1+lzVrGo0yJnc7Zrf3HTlkEnYDJIgYSwUJO9rPs09Dwv09Lc6e9OovXjbXXw0oK8VmXZv8itkwWrzHK1jz1txPCCx21MvXfVK3Ft2Kk2Xa44YYyd6epzM8ERqV2wDvRo9tKn+RGvX2wxr+6fu3s6ckAgABAAAAAMAAYAATlZN8E2B57sx5sR31JSfhd/IoRG+o/LTvOum/CzN6rub+RpwyRTfqyUNyiEnYBpkDKMyBuc9LhKrbc2ZB4ilXTtrLNBbp1LdGffa/oyj1WOI9UNDo8szuk/HhybTlaNl1lGWhVfcPDLCEfdhGPkkjYjiGFadzMthKCAQCAAAAAAGAEhkAA59oytSlbe1ZeJMCq4XDZcdiEt1Jvzklb0bKmOn+/M/RczX/wCNWPqlU7yl2Wj42v8AMvqB0Fd9iCIdBykAImAEjZFXTXE5ENtWP1T4VGvOL/Qq9X7I+650U+ufsjq9PPWoQ96rTj5yVyhWN3iGladY7T/C+s1mIxAAEAgAAAAABgBIYAQOXF9KdOH3szXYv+GSIyVDLicVP/MqUrdqVGPzbOaV9Vpd5L7rWv03/dpozum/fk2vwt6elj2eEu2lCyIkZkJIBEgEjZAgR20oad00/j+p4dT+msdLP+5+JcOAp5sbQ7JTl5Qb+JRwRvLDRzzrDK4mkySAQAAgABAMAAYASAgMAAhdo1b85Jb30Y976KO4cNeGp7uCVkdIddjlIATAAHFAZIDl2nDo37Y/E8c8bpL36fjJCP2V/G0+yNT8jKPT/qtDqP0Z/C2GiyyAQCAAABAMAAAGAAMDCs3leVXdrJaL4kiBq3cpQksqpSTlJtWbcVJLwUkdRO3MxpqW3MHF2li8LFrRp16Sa9SUalIUq0JrNTnGcXulCSlF+KIGxAAGMgOd7Uw0XaWJoRfCVamn6slOmcNpYeXs4ig+6tTfzINN2aFWDUJwnp9mSl8CJjcaTE6naJ2RG2MjffarHxUf6Gfhjty6n+Wnnt3Ydx/C0l5mhgIBAACAAGAAADAAABgeS8sMXUqVavOSllVSWWGqiknZacbI9I8JhQ9oSShVfFJLx0IlKyclaFZ0aXMuaeVNOLcbX13omETK+bPwOMaTnjKkfupRm/OSYcbhO0ackrSqTm+MlBP/AGpBDk2jsqNdWnOpb3VN5X3x3MG0HieTWX2LSXCSVyduu5AbUwGSE70oqWV2eVb7BMSoXJxunUhJNxlCok3F2aalrqjmHT27Z9eS5mrU1lziTl1tXytvtsynn1TLErOH14rVW4sqhAACAAEAAMAAAGAAAAB5nWw1WpKo6ihVpOcml7FWnr1PdJdjt3lPH1U1mYnw0MnTRaImvEqjtXk3zlRRoSvmqLNB+3TTaV3Hhfr3dpdret43WVO9LUnVoem7I2ZChThTgtIRUVx0R28JS1NaCRmQHYDGaAjtoYONRNNb0SPLZcmJ08bWpR0i/wB9TfZK915p+gh6RPD0fFJrDw6uhe/F2TuUus+JW+i8zC4p3146llTAAAgABAADAAABgAAAAUvCx0qLhOa9WZUxzLYidxDRsOgnVxE3vtTpxfFOTnL/ANcPMtdJXzKr1tuIhYacS+zm2IkMB3ICYGqfaBXuVGzqk1Crh5KnUpys5OKlenLevPLr3nnltNa7h7dPFbX1ZxYDno0qkK0pSUU8t3dLjYzsl7WjmWnSlaz6Yeix3LuRpMlkAgAAAQAAAMAAAGAAICm5sqrt9VSp+ZmVf3T92vTmtfs6djU48zTml9bGNRtdbkr/AAsvA08Fe2kMzqL92Sf+kpA93gcSAMgKLAT0AHqBqxFPNGUfei16EWjcTDqtu2YlDYlXpS0s7O/YZd2vSeV0NJkgAAAABAIBgMAAAGAgACk7SX7rFte/VfqzLye6fu18U8V+zt5P9DDYem/sUKMb9qgka1OIiGRkndplKpHbgJ6HIQGD0ZI2EDW9CRjMgRuKp/WRXWs3mUOorqZaXTX3WNrTSlmjF8Yxfmi3HhRmNTpmSgAACATAAGAwAAAAABMCo4KnzlOrGS6Us6f4nf5mZHMztqzxEadWzqdqcY69FJa79DVr4ZNvLvpPqZ05ZWIA0BjJEjGLsBkyBraA46sbzf8Ap6vzKvUxyu9LPErBgX+6pf6VP8qPWvthXv7pbjpyAABAJgADAAGAAAAAAVXBT6dR8ak35yZm/vlqTHoj7O5e07ddn5/8GhhndWdmjVm+J7PFsIAAmgMZRJGNrEDGQHJVXtvhC3qVuoW+mTmB+qpW/wAqn+VHpT2w8b+6fu3nTkAACAQAAwABgAAAAYydk3vsr24ghVcFTc6PQazynF3d7Jp5n8GZ9K9zUvPbPPh2Ql0n+GPxkXOn8SpdT5htrYhQjfe3olxZ6ZMkUruXjjxzktqG+nNSSktzR1WYmNw4tWazqWTZKCuSC4CbIGqowOGVVOVSN91ODt3uX6FbPK300cb/AJTeyJ5qFN8I5f8AtbXyO8c7rDzzRq8uw7eYAAEAgABgAAAwABAICvYeap1qlN6Xm3bxadv5WmU4ntu0Nd+OJ/z/ADbkwGKpzk4wnmcYqM96cZRlKLTT1T03PU98FdRw8Oqncxs9qVOlTXZJ/A8usn2x93p0NfdP2duzav2eOq7+v++xnXS3/a46vH+6HbJ23+ZbUi7iQiAmBorMCuqtmq42WsfqMNC/25qObMv/ACpfylTNPqaHTU9MSueyqeWjH715eDenoeuONVV807vLsO3kAEAgABAMBgAAAAACA48ds+nW1krSW6pHSS8es4tjrby9MeW1PCIey6lKoqkpQmrOLqJOE3wTjufXrfwIxY5rPl6Zs0ZK+OXBtiXTp/hl8UV+t81/L36Dxb8NuEqtWa3ppnhjt2zErGWndEwnMyaTW5pNdqZrRO2NManUuebyu63PeuBKGyM7kAbA5qrA49nbDrSlOVdqMZV5VoaWlG8ciW/Xorrtq3v6q00taZ3xDQjNTHWIrzOlqikkktElZLgj2UmQAAgEAAIBgMAAAABAIBMDViVeEl91kwiVP24+nS/DP0a/Up9b+38tD/58+78f+nhZXRTqu2TWzat4uPXF3X4X/W/oaXTX3XX0ZXVU1bu+rdUXUWVVoi7EjNzIBhaeeaT3b33I5khMnLs0AwBgACAQAAwAAAYAAgEwMWAmBReUs8mIw9N/ahi14p0mvmV+rjdYlb6CdWtB4Oehnw07QkcPXyTjL7Psy/C+vwdn4FnDftvCt1GPvpMfPlM1DSY7nnEkYNNAdmzIe1LwXz+RzKYSCOXRgMBgJgACAAGAAAAAAACYGICYHn30kz5rEbOqdUsRODf4oJW9EzzzV7qPfprduQ6MrMy2xHMOyM8ya4ncOZjSa2ZiOcpJP2qbyS7bLR+Kt43NPBfvp/LG6nF/TyTHxPLaz1eDVVkBLYankjGPWlr39Zw7bQMkAwAAAAEAAADAAAAAQCYCAQFB+l3D58Ph2t8Kk5p8Gok63DqvlxYCvztKnU9+nGTXBtarzMnJXVpht4rd1Yl106lmcRL11tKbLrZa9uqrBr+aPSXpm8y30t9X7fqodbj3j7vp/ZLV3pfwZosksJHNOPffy1IlMJc4dGgGAwGAAIAAAAAAYAAAIBMDEBAVfl7SzUqN9zqTj5wZMJhU+TC/cKL3wnOHrm/+jO6mNXa3S29CSvrbgVVyPDqqvIoTT1g4z8ne3oelbdsxLztXvras/Kxzs7rqZsvnWzZ1O03fqi/VkSmEicujQGSAYDAAEAAIBgAAAAAABiwEAgK7y1+qor/rr8rJg+JVXYdJwzvqlVb/ANqRR6v3Q1Okj0yk8TR0c1w1KcwuVnnSH5Q43mMLOb1kqbt2ytoTWu507me2Jld6N8kL78kb+SNt8zPl34Dc33I5l1DrISaAyQDAAAAAAEAwAAAAABAJgJgICB5X070qUvdrLycX+hMJhBUKdrR4aePWZWW3daZbGKvbSIbK7srX0POXvVSOV+IlVqU8PHXNJLvk3livNntijbjLbX93rGWytwsjUfPu/CRtBdupDqG8hJoDIBgAAAAIAAAGAAAAAgEAgEBF7foKdPNO+Wk86Sds1TdG/Yrs4yTqsy9MUbvEK1Qeplz5bPwyxK0IdVlWo0aK2jRlXnGnCklXbld5pqXQjp2pPwZZw6iY2r9RNprPbG58Lvhdt0KtWFGjKVSU5JXjFqMV1u77OBc/rVmdQzv9NeKza3ELIkdvEwGgMkAwAAAAEAAAAAwAAAQCAAEBEcpp2opLrqL4M8Oon0LPSRu6vYcz2oWJegTCo7Y/i6vY4RXZaEUelvKMft2s/IbCr9oze5CT9LfM9enj1q/WTrGv5eZQCWSCDQDAAAAAQAB//9k=" alt="" />
                <center style={{marginTop:'10px'}}><span className="subCategories" style={{fontSize:'23px',fontWeight:'bold'}}>One Shoulder</span></center>
                <center style={{marginTop:'10px'}}><span className="subCategories" style={{fontSize:'15px',fontWeight:'bold'}}>Shop All</span></center>
              </Grid.Column>
              <Grid.Column width={8}>
                <Link to='/mWomenCategory/LACE' style={{color:"black"}}>

                <Image src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSBCJ67RENHj3gC0Gnyj04NY-QMIKySTIqcOb-stYOLheo2yjv3" alt="" />
                <center style={{marginTop:'10px'}}><span className="subCategories" style={{fontSize:'23px',fontWeight:'bold'}}>Lace</span></center>

                <center style={{marginTop:'10px'}}><span className="subCategories" style={{fontSize:'15px',fontWeight:'bold'}}>Shop All</span></center>
              </Link></Grid.Column>

            </Grid.Row>

            <Grid.Row style={{marginLeft:'10px',marginRight:'10px'}}>
              <Grid.Column width={8}>
                <Image src="https://xo.lulus.com/images/product/xlarge/1201034_186906.jpg" alt="" />
                <center style={{marginTop:'10px'}}><span className="subCategories" style={{fontSize:'23px',fontWeight:'bold'}}>Bell Sleve</span></center>
                <center style={{marginTop:'10px'}}><span className="subCategories" style={{fontSize:'15px',fontWeight:'bold'}}>Shop All</span></center>
              </Grid.Column>
              <Grid.Column width={8}>
              <Image src="https://xo.lulus.com/images/product/xlarge/1983492_340182.jpg" alt="" />
              <center style={{marginTop:'10px'}}><span className="subCategories" style={{fontSize:'23px',fontWeight:'bold'}}>Embroidery</span></center>
              <center style={{marginTop:'10px'}}><span className="subCategories" style={{fontSize:'15px',fontWeight:'bold'}}>Shop All</span></center>
              </Grid.Column>
            </Grid.Row>

            <Grid.Row style={{marginTop:'20px'}}>
              <Grid.Column width={16}>
                <center><span className="brandCategories" style={{fontSize:'20px',fontWeight:'bold'}}>Shop by Products</span></center>
              </Grid.Column>
            </Grid.Row>

            <Grid.Row>
              <Grid.Column width={16}>
              <List selection verticalAlign='middle'>
                  <List.Item>
                    <List.Content floated='right'>
                      <Icon name="chevron right" size="large"/>
                    </List.Content>
                    <Image avatar src='https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQeLwSiBAmnVxgqxEBaRYGOCqtddZoYHHmLXvsjH-q1hibCIC80' alt="" />
                    <List.Content>
                      <List.Header>Bags</List.Header>
                    </List.Content>
                  </List.Item>
                  <List.Item>
                    <List.Content floated='right'>
                      <Icon name="chevron right" size="large"/>
                    </List.Content>
                    <Image avatar src='https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRfbdCWFIy2CUHd-eavIU6YDugsREZoxuHbMlp9OSd101vXfziH' alt="" />
                    <List.Content>
                      <List.Header>Shoes</List.Header>
                    </List.Content>
                  </List.Item>
                  <List.Item>
                    <List.Content floated='right'>
                      <Icon name="chevron right" size="large"/>
                    </List.Content>
                    <Image avatar src='http://cdn.watchshop.com/profiler/thumb_cache/600s/99944281_v_1423897974.jpg' alt="" />
                    <List.Content>
                      <List.Header>Daniel</List.Header>
                    </List.Content>
                  </List.Item>
                </List>
              </Grid.Column>
            </Grid.Row>

            <Grid.Row >
              <Grid.Column width={16}>
                <Segment style={{backgroundColor:'#233c5b'}}>
                 <center style={{marginTop:'10px'}}><span className="subCategories" style={{fontSize:'23px',fontWeight:'bold',color:'white'}}>Follow Us On</span></center>
                 <center>
                  <Icon name="facebook" style={{color:'white',marginTop:'15px',marginRight:'15px'}} size="large"/>
                  <Icon name="twitter" style={{color:'white',marginTop:'15px',marginRight:'15px'}} size="large"/>
                  <Icon name="google" style={{color:'white',marginTop:'15px',marginRight:'15px'}} size="large"/>
                 </center>
                </Segment>
              </Grid.Column>
            </Grid.Row>

          </Grid></div>
        )
      }
         </div>
      );
   }
}
